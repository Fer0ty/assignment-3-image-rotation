//
// Created by Артемий Соловьев on 17.12.2022.
//

#include "bmp.h"


static const size_t BMP_HEADER_SIZE = sizeof(struct bmp_header);
static const uint16_t SIGN = 0x4d42;
static const int64_t ZERO_BYTE = 0;

size_t get_padding_size(const struct image* image) { //размер отступов
    if ((image->width * 3) % 4) {
        return 4 - (image->width * 3) % 4;
    } else {
        return 0;
    }
}

size_t get_image_size(const struct image* image) { //размер изображения
    return image->height * (image->width + get_padding_size(image)) * 3;
}

size_t get_new_bmp_size(const struct image* image) {
    return BMP_HEADER_SIZE + get_image_size(image);
}

struct bmp_header get_bmp_header(const struct image* image) {
    struct bmp_header header = {
            .bfType = SIGN,
            .bfileSize = get_new_bmp_size(image),
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_SIZE, // file offset to pixel array
            .biSize = 40,
            .biWidth = image->width, // image width
            .biHeight = image->height, // image
            .biPlanes = 1, // always 1
            .biBitCount = 24, // bit per pixel
            .biCompression = 0,
            .biSizeImage = get_image_size(image), // image size
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

static const char* const read_debug_msg[] = { //read debug
        "",
        "Invalid signature",
        "Invalid bits",
        "Invalid bmp header",
        "Invalid offset in header",
        "Invalid pixel: can't read",
        "File is not opened"
};

static const char* const write_debug_msg[] = { //write debug
        "",
        "Invalid bmp header",
        "Invalid pixel array",
        "File is not opened"
};

bool file_for_write(FILE** filepath,  const char* filename) { //открывает файл на запись
    *filepath = fopen(filename, "wb");
    return *filepath != NULL;
}

bool file_for_read(FILE** filepath,  const char* filename) { //открывает файл на чтение
    *filepath = fopen(filename, "rb");
    return *filepath != NULL;
}

bool is_file_open(FILE* filepath) { // Открыт ли файл
    return filepath != NULL;
}

void read_debug_status(enum read_status status) { //debug статус на чтение
    if (status) fprintf(stderr, "Read error in bmp function: %s", read_debug_msg[status]);
}

void write_debug_status(enum write_status status) { //debug статус на запись
    if (status) fprintf(stderr, "Write error in bmp function: %s", write_debug_msg[status]);
}

void print_pixel(FILE* filepath, const struct pixel* pixel) {
    putc(pixel->b, filepath);
    putc(pixel->g, filepath);
    putc(pixel->r, filepath);
}

void print_padding(FILE* filepath, size_t padding_size) {
    fwrite(&ZERO_BYTE, 1, padding_size, filepath);
}


enum write_status write_pixel_array(FILE* filepath, const struct image* image) { //
    size_t padding_size = get_padding_size(image);
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            print_pixel(filepath, get_pixel_pattern(image, i, j));
        }
        print_padding(filepath, padding_size);
    }
    return ferror(filepath) ? WRITE_PIXEL_ARRAY_ERROR : WRITE_OK;
}



enum write_status check_write(FILE* filepath) {
    return is_file_open(filepath) ? WRITE_OK : WRITE_FILE_IS_NOT_OPENED;
}


enum write_status write_header_to_file(FILE* filefath, const struct image* image) {
    const struct bmp_header header = get_bmp_header(image);
    if (fwrite(&header, BMP_HEADER_SIZE, 1, filefath)) {
        return WRITE_OK;
    } else {
        return WRITE_HEADER_ERROR;
    }
}



enum write_status to_bmp(FILE* filepath, const struct image* image) {
    enum write_status status;
    status = check_write(filepath);
    if (status) return status;
    status = write_header_to_file(filepath, image);
    if (status) return status;
    status = write_pixel_array(filepath, image);
    return status;
}

enum write_status write_bmp(char* filename, struct image* image) {
    FILE* filepath;
    file_for_write(&filepath, filename);
    enum write_status status = to_bmp(filepath, image);
    fclose(filepath);
    return status;
}

enum write_status write_bmp_and_image(char* filename, struct image* image) {
    enum write_status status = write_bmp(filename, image);
    dump_image(image);
    return status;
}

enum read_status check_file_for_read(FILE* filepath) {
    return is_file_open(filepath) ? READ_OK : READ_FILE_IS_NOT_OPENED;
}

enum read_status read_bmp_header(FILE* filepath, struct bmp_header* header) {
    if (fread(header, BMP_HEADER_SIZE, 1, filepath)) {
        return READ_OK;
    } else {
        return READ_INVALID_HEADER;
    }
}

enum read_status check_bmp_header(struct bmp_header* header) {
    if (header->bfType != SIGN) return READ_INVALID_SIGNATURE;
    if (header->bOffBits >= header->bfileSize) return READ_INVALID_BITS;
    return READ_OK;
}

enum read_status read_pixel(FILE* filepath, struct pixel* pixel) {
    uint8_t color_bgr[3];
    size_t size = fread(color_bgr, 3, 1, filepath);
    if (!size) return READ_INVALID_PIXEL;
    pixel->b = color_bgr[0];
    pixel->g = color_bgr[1];
    pixel->r = color_bgr[2];
    return READ_OK;
}

enum read_status skip_padding(FILE* filepath, long padding_size) {
    if (fseek(filepath, padding_size, SEEK_CUR)) {
        return READ_INVALID_OFFSET;
    } else {
        return READ_OK;
    }
}

enum read_status read_pixel_array(FILE* filepath, struct image* image) {
    long padding_size = (long) get_padding_size(image);
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            struct pixel px;
            if (read_pixel(filepath, &px)) return READ_INVALID_PIXEL;
            set_pixel(image, px, i, j);
        }
        enum read_status status = skip_padding(filepath, padding_size);
        if (status) return status;
    }
    return READ_OK;
}

enum read_status read_image(FILE* filepath, const struct bmp_header* header, struct image* image) {
    initialize_image(image, header->biWidth, header->biHeight);
    if (fseek(filepath, (long) header->bOffBits, SEEK_SET)) return READ_INVALID_OFFSET;
    return read_pixel_array(filepath, image);
}

enum read_status from_bmp(FILE* filepath, struct image* image) {
    struct bmp_header header;
    enum read_status status;
    status = check_file_for_read(filepath);
    if (status) return status;
    status = read_bmp_header(filepath, &header);
    if (status) return status;
    status = check_bmp_header(&header);
    if (status) return status;
    status = read_image(filepath, &header, image);
    return status;
}

enum read_status read_bmp(char* filename, struct image* image) {
    FILE* filepath;
    file_for_read(&filepath, filename);
    enum read_status status = from_bmp(filepath, image);
    fclose(filepath);
    return status;
}
