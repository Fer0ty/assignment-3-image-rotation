//
// Created by Артемий Соловьев on 17.12.2022.
//

#include "image.h"

bool pixel_position(const struct image* image, size_t top_px, size_t left_px) {
    return image->width > left_px && image->height > top_px;
}

struct pixel* get_pixel_pointer(const struct image* image, size_t position_top, size_t position_left) {
    if (pixel_position(image, position_top, position_left)) {
        struct pixel* pixel = image->data;
        pixel = pixel + position_top * image->width + position_left;
        return pixel;
    } else {
        return NULL;
    }
}
struct pixel get_pixel(const struct image* image, size_t position_top, size_t position_left) {
    struct pixel* pixel_ptr = get_pixel_pointer(image, position_top, position_left);
    return *pixel_ptr;
}

void initialize_image(struct image* orig, size_t width, size_t height) {
    orig->height = height;
    orig->width = width;
    orig->data = (struct pixel*) malloc(width * height * sizeof(struct pixel));
}


void draw_image(struct image* image, size_t offset, const uint8_t* data, size_t size) {
    for (size_t i = 0; i < size/3; i++) {
        image->data[offset+i] = (struct pixel) {data[(i*3)], data[(i*3)+1], data[(i*3)+2]};
    }
}

bool set_pixel(struct image* image, struct pixel pixel,
               size_t position_top, size_t position_left) {
    if (pixel_position(image, position_top, position_left)) {
        struct pixel* addr = get_pixel_pointer(image, position_top, position_left);
        *addr = pixel;
        return true;
    } else {
        return false;
    }
}

void dump_image(struct image* image) {
    if (image != NULL) {
        free(image->data);
    }
}

