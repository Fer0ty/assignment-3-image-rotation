//
// Created by Артемий Соловьев on 16.12.2022.
//

#include "rotate.h"

struct image rotate( struct image const image_to_rotate ) {
    struct image rotated_image;
    initialize_image(&rotated_image, image_to_rotate.height, image_to_rotate.width);
    for (size_t i = 0; i < image_to_rotate.height; i++) {
        for (size_t j = 0; j < image_to_rotate.width; j++) {
            set_pixel(&rotated_image, get_pixel(&image_to_rotate, i, j),
                      j, image_to_rotate.height-1-i);
        }
    }
    return rotated_image;
}
