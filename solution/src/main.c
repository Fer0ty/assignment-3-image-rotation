//
// Created by Артемий Соловьев on 16.12.2022.
//

#include "bmp.h"
#include "rotate.h"


int main(int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc == 3) {
        struct image img;
        read_debug_status(read_bmp(argv[1], &img));
        struct image new_img = rotate(img);
        dump_image(&img);
        write_debug_status(write_bmp_and_image(argv[2], &new_img));
    }
    return 0;
}
