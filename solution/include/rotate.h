#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"

struct image rotate( struct image const image_to_rotate);

#endif
