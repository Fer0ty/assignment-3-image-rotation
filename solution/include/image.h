#ifndef IMAGE_H
#define IMAGE_H
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t r, g, b;
};

struct image {
    size_t width, height;
    struct pixel* data;
};

struct pixel* get_pixel_pattern(const struct image* image, size_t position_top, size_t position_left);
struct pixel get_pixel(const struct image* image, size_t position_top, size_t position_left);
void initialize_image(struct image* orig, size_t width, size_t height);
bool set_pixel(struct image* image, struct pixel pixel, size_t position_top, size_t position_left);
void dump_image(struct image* image);

#endif
